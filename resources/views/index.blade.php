<!DOCTYPE html>
<html lang="zxx">
    <head>
        <!-- META DATA -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- TITLE OF SITE -->
        <title>Damson-Your Vision, Your Future</title>

        <!-- favicon -->
        <link id="favicon" rel="icon" type="image/png" href="assets/images/favicon.ico">

        <!-- Google Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500%7CMontserrat:300,400,500,600,700,800,900">

        <!-- Bootstrap css -->
        <link rel="stylesheet" href="{{asset('akash/asset/css/bootstrap.css')}}">

        <!-- animate.css -->
        <link rel="stylesheet" href="{{asset('akash/asset/css/animate.css')}}">

        <!-- magnific-popup.css -->
        <link rel="stylesheet" href="{{asset('akash/asset/css/magnific-popup.css')}}">

        <!-- owl carousel css -->
        <link rel="stylesheet" href="{{asset('akash/asset/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('akash/asset/css/owl.theme.min.css')}}">

        <!-- font-awesome.min.css -->
        <link rel="stylesheet" href="{{asset('akash/asset/css/font-awesome.min.css')}}">
        
        <!-- Main style css -->
        <link rel="stylesheet" href="{{asset('akash/asset/css/style.css')}}">
        
        <!-- Responsive css -->
        <link rel="stylesheet" href="{{asset('akash/asset/css/responsive.css')}}">
         
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="70">


        <!--Start Preloader-->
        <div class="page-loader startLoad">
          <div class="loader startLoad"></div>
        </div>
        <!--End Preloader-->


        <!--Start Scroll Top-->
        <div class="top">
            <a href="#home" class="smooth_scroll"><i class="fa fa-angle-up fa-2x"></i></a>
        </div>
        <!--End Scroll Top-->


        <!--Start Home Section-->
        <section class="home svg_shape" id="home">
            <div class="full_height bg_image type" style="background-image: url('{{asset('akash/asset/images/me-1.jpg')}}');" data-stellar-background-ratio="0.7">
                
                <!-- nav -->
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <!-- LOGO -->
                            <img src="{{asset('akash/asset/images/logo.png')}}">

                            <a class="navbar-brand" href="#"></a>

                           <link rel="logo" href="{{asset('akash/asset/images/logo.png')}}" type="image/png" sizes="160x16">
                        

                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="#home" class="smooth_scroll">Home</a>
                                </li>
                                <li>
                                    <a href="#about" class="smooth_scroll">About</a>
                                </li>
                                <li>
                                    <a href="#services" class="smooth_scroll">Services</a>
                                </li>
                                <li>
                                    <a href="#portfolio" class="smooth_scroll">Portfolio</a>
                                </li>
                                <li>
                                    <a href="#blog" class="smooth_scroll">Blog</a>
                                </li>
                                <li>
                                    <a href="#testimonials" class="smooth_scroll">Testimonials</a>
                                </li>
                                <li>
                                    <a href="#contact" class="smooth_scroll">Contact</a>
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!--.container-->
               </nav>
                <div class="display-table">
                    <div class="display-table-cell">
                        <h1>I AM <span id="typed"></span></h1>
                        <div id="typed-strings">
                            <span>Ashraful Islam</span>
                            <span>Apps Developer </span>
                            <span>Form Bangladesh</span>
                        </div>
                        <ul class="text-center list-unstyled social">
                            <li>
                                <a href="https://www.facebook.com/ashrafulislam.akash.1232/" target="_blank">
                                    <i class="fa fa-facebook fa-2x"></i> 
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/DesignerAkash1" target="_blank">
                                    <i class="fa fa-twitter fa-2x"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/in/ashraful-islam-akash/" target="_blank">
                                  <i class="fa fa-linkedin fa-2x"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.behance.net/ashrafulislamakash"target="_blank">
                                   <i class="fa fa-behance" aria-hidden="true"></i>                           
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/designerakash/"target="_blank">
                                    <i class="fa fa-instagram fa-2x"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="go_down scroll_svg">
                    <a href="#about" class="smooth_scroll">
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <div class="svg_divider">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                        <path d="M761.9,44.1L643.1,27.2L333.8,98L0,3.8V0l1000,0v3.9"></path>
                    </svg>
                </div>
            </div><!--.full_height-->
        </section>
        <!--End Home Section-->


        <!--Start About Section-->
        <section class="about padding-top-90 padding-bottom-90" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 about-img">
                        <div class="my__img animated animated_scroll fadeInUp" style="animation-delay: 0.3s">
                            <img src="{{asset('akash/asset/images/me.jpg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-7 padding-left-35">
                        <h3 class="animated animated_scroll fadeInUp" style="animation-delay: 0.3s">Hi There! I am Md. Ashraful Islam</h3>
                        <p class="animated animated_scroll fadeInUp" style="animation-delay: 0.3s">student of Bachelor of Science (BSc.) program in Computer Science and Engineering (CSE) at World University of Bangladesh (WUB). which is the most prestigious university and holds the topmost position for engineering in Bangladesh.</p>
                        <p class="animated animated_scroll fadeInUp" style="animation-delay: 0.3s">My long term career objective is to pursue a Programming career, either as a faculty in a university or as a Progmar in an industry where I can devote myself to the field of Programming and Development.</p>

                        <!-- Skills -->
                        <div class="skills">

                            <!--skill 1-->
                            <div class="skill">
                                <span>Adobe Photoshop</span>
                                <span>85%</span>
                                <div class="progress_bg">
                                    <div class="progress_bar"></div>
                                </div>
                            </div>

                               <!--skill 1-->
                            <div class="skill">
                                <span>Adobe Illustrator</span>
                                <span>95%</span>
                                <div class="progress_bg">
                                    <div class="progress_bar"></div>
                                </div>
                            </div>

                                <!--skill 1-->
                            <div class="skill">
                                <span>adobe XD</span>
                                <span>90%</span>
                                <div class="progress_bg">
                                    <div class="progress_bar"></div>
                                </div>
                            </div>


                            <!--skill 1-->
                            <div class="skill">
                                <span>UX/UI Design</span>
                                <span>85%</span>
                                <div class="progress_bg">
                                    <div class="progress_bar"></div>
                                </div>
                            </div>

                        
                            <!--skill 4-->
                            <div class="skill">
                                <span>Android Studio</span>
                                <span>60%</span>
                                <div class="progress_bg">
                                    <div class="progress_bar"></div>
                                </div>
                            </div>


                            <!--skill 4-->
                            <div class="skill">
                                <span>Kotlin</span>
                                <span>40%</span>
                                <div class="progress_bg">
                                    <div class="progress_bar"></div>
                                </div>
                            </div>


                        </div><!--.skills-->
                        <div class="about_btns animated animated_scroll fadeInUp" style="animation-delay: 0.3s">
                            <a href="CV/Md. Ashraful Islam.pdf"; target="_blank">Download CV</a>
                            <a href="#contact" class="smooth_scroll">Contact Me</a>
                        </div>



                    </div><!--.col-sm-7-->
                </div><!--.row-->
            </div><!--.container-->
        </section>
        <!--End About Section-->


        <!--Start Services Section-->
        <section class="services padding-top-90 padding-bottom-90 primary_bg" id="services">
            <div class="container">
                <div class="sec_title">
                    <h2>services</h2>
                </div>

                <div class="row">
                    
                    <!--Service 1-->
                    <div class="col-md-3 col-sm-6 item animated animated_scroll fadeInUp" style="animation-delay: 0.3s">
                        <div class="content" style="height: 400px;">
                            <div class="icon">
                             <span><i class="fa fa-android"></i> </span>
                            </div>
                            <h3>Android Application</h3>
                            <p>Making android application is like a hobby. I have made 10+ android applications with core android and also with latest coding language Kotlin.</p>
                        </div>
                    </div>
                    
                    <!--Service 2-->
                    <div class="col-md-3 col-sm-6 item animated animated_scroll fadeInUp" style="animation-delay: 0.6s">
                        <div class="content"style="height: 400px;">
                            <div class="icon">
                                <i class="fa fa-magic"></i> 
                            </div>
                            <h3>Graphic Design</h3>
                            <p>Being a Graphic designer was a dream from my childhood. I have done many Design projects.</p>
                        </div>
                    </div>




                    <!--Service 3-->
                    <div class="col-md-3 col-sm-6 item animated animated_scroll fadeInUp" style="animation-delay: 1.2s">
                        <div class="content"style="height: 400px;">
                            <div class="icon">
                            
                                <i class="fa fa-lightbulb-o"></i>
                            </div>
                            <h3>UX/UI Design</h3>
                            
                            <p>creative, reliable, hard-working designer. I have designed websites, mobile apps and games UI design.</p>
                        </div>
                    </div>
                    
                    <!--Service 4-->
                    <div class="col-md-3 col-sm-6 item animated animated_scroll fadeInUp" style="animation-delay: 0.9s">
                        <div class="content"style="height: 400px;">
                            <div class="icon">
                                <i class="fa fa-camera"></i>
                            </div>
                            <h3>Photography</h3>
                            <p>Professional IMAGE PROCESSING Service Provider, I have Good experience in IMAGE  EDITING work.</p>
                        </div>
                    </div>
                    
                    
                </div><!--.row-->
            </div><!--.container-->
        </section>
        <!--End Services Section-->


        <!--Start Portfolio Section-->
        <section class="portfolio padding-top-90 padding-bottom-90" id="portfolio">
            <div class="container">
                <div class="sec_title">
                    <h2>portfolio</h2>
                </div>
                <ul class="list-unstyled ul-filter">
                    <li data-filter="*" class="active_filter">all</li>
                    <li data-filter=".marketing">Android Application</li>
                    <li data-filter=".photography">photography</li>
                    <li data-filter=".design">Graphic Design</li>
                    <li data-filter=".design">UX/UI Design</li>
                </ul>
                <div class="row grid">
                    
                    <!--project 1-->
                    <div class="col-xs-12 col-sm-6 col-md-4 grid-sizer grid-item design">
                        <div class="box animated animated_scroll fadeInUp" style="animation-delay: 0.3s">
                            <div class="my_img" data-mfp-src="assets/images/portfolio/work-1.jpg">
                                <img src="{{asset('akash/asset/images/portfolio/work-1.jpg')}}" alt=''>
                            </div>
                            <div class="boxContent">
                                <div class="display-table">
                                    <div class="display-table-cell">
                                        <span class="post">Graphic Design</span>
                                        <h3 class="title">Illustration</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--project 2-->
                    <div class="col-xs-12 col-sm-6 col-md-4 grid-item marketing">
                        <div class="box animated animated_scroll fadeInUp" style="animation-delay: 0.3s">
                            <div class="my_img" data-mfp-src="assets/images/portfolio/work-2.jpg">
                                <img src="{{asset('akash/asset/images/portfolio/work-2.jpg')}}" alt=''>
                            </div>
                            <div class="boxContent">
                                <div class="display-table">
                                    <div class="display-table-cell">
                                        <span class="post">UX/UI Design</span>
                                        <h3 class="title">Darebux Mobile Apps UI Design</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--project 3-->
                    <div class="col-xs-12 col-sm-6 col-md-4 grid-item photography">
                        <div class="box animated animated_scroll fadeInUp" style="animation-delay: 0.3s">
                            <div class="my_img" data-mfp-src="assets/images/portfolio/work-3.jpg">
                                <img src="{{asset('akash/asset/images/portfolio/work-3.jpg')}}" alt=''>
                            </div>
                            <div class="boxContent">
                                <div class="display-table">
                                    <div class="display-table-cell">
                                        <span class="post">Photography</span>
                                        <h3 class="title">Color Correction</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--project 4-->
                    <div class="col-xs-12 col-sm-6 col-md-4 grid-item design">
                        <div class="box animated animated_scroll fadeInUp" style="animation-delay: 0.4s">
                            <div class="my_img" data-mfp-src="assets/images/portfolio/work-4.jpg">
                                <img src="{{asset('akash/asset/images/portfolio/work-4.jpg')}}" alt=''>
                            </div>
                            <div class="boxContent">
                                <div class="display-table">
                                    <div class="display-table-cell">
                                        <span class="post">Graphic Design</span>
                                        <h3 class="title">Business Flayer Design</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--project 5-->
                    <div class="col-xs-12 col-sm-6 col-md-4 grid-item photography">
                        <div class="box animated animated_scroll fadeInUp" style="animation-delay: 0.4s">
                            <div class="my_img" data-mfp-src="assets/images/portfolio/work-5.jpg">
                                <img src="{{asset('akash/asset/images/portfolio/work-5.jpg')}}" alt=''>
                            </div>
                            <div class="boxContent">
                                <div class="display-table">
                                    <div class="display-table-cell">
                                        <span class="post">Photography</span>
                                        <h3 class="title">project name</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!--project 6-->
                    <div class="col-xs-12 col-sm-6 col-md-4 grid-item marketing">
                        <div class="box animated animated_scroll fadeInUp" style="animation-delay: 0.4s">
                            <div class="my_img" data-mfp-src="assets/images/portfolio/work-6.jpg">
                                <img src="{{asset('akash/asset/images/portfolio/work-6.jpg')}}" alt=''>
                            </div>
                            <div class="boxContent">
                                <div class="display-table">
                                    <div class="display-table-cell">
                                        <span class="post">Marketing</span>
                                        <h3 class="title">project name</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!--.row-->
            </div><!--.container-->
        </section>
        <!--End Portfolio Section-->


        <!--Start Blog Section-->
        <section class="blog padding-top-90 padding-bottom-90 primary_bg" id="blog">
            <div class="container">
                <div class="sec_title">
                    <h2>Blog</h2>
                </div>
                <div class="row">

                    <div class="col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-4 item animated animated_scroll fadeInUp" style="animation-delay: 0.3s">
                        <div class="blog__img">
                            <img src="{{asset('akash/asset/images/blog/blog-1.jpg')}}" alt="">
                        </div>
                        <div class="content"style="height: 270px;">
                            <span class="bold">March 19, 2019</span>
                            <a href="#"><h3>Hacking কি, কারা Hacker, কেনো Hacking করে।</h3></a>
                            <p>সবাই হ্যাকার ( HACKER ) সম্পর্কে কম বেশি জানেন। আর হ্যাকার ( HACKER ) নিয়ে ইন্টারনেট অনেক আর্টিকেল আছে। আর্টিকেল থাকলে কি হবে আমরা অনেকেই তা জানি না বা ঠিক ভাবে বুঝতে পারি না।  ...</p>
                        </div>
                    </div>

                    <div class="col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-4 item animated animated_scroll fadeInUp" style="animation-delay: 0.6s">
                        <div class="blog__img">
                            <img src="{{asset('akash/asset/images/blog/blog-2.jpg')}}" alt="">
                        </div>
                        <div class="content"style="height: 270px;">
                            <span class="bold">March 19, 2019</span>
                            <a href="#"><h3>Computer Networking এবং Administration কি?</h3></a>
                            <p>Computer Networking কি? সহজ ভাষায়, এক বা একাধিক কম্পিউটারে নিজেদের মধ্যে আন্ত সংযোগ প্রক্রিয়াকে কম্পিউটার নেটওয়ার্ক বলা হয়। অন্যভাবে বলা যায় কম্পিউটার নেটওয়ার্ক বা নেটওয়ার্কিং হচ্ছে ...</p>
                        </div>
                    </div>

                    <div class="col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-4 item animated animated_scroll fadeInUp" style="animation-delay: 0.9s">
                        <div class="blog__img">
                            <img src="{{asset('akash/asset/images/blog/blog-3.jpg')}}" alt="">
                        </div>
                        <div class="content"style="height: 270px;">
                            <span class="bold">March 19, 2019</span>
                            <a href="#"><h3>Web Design এবং Web Development কি?</h3></a>
                            <p>Web Design মানে হচ্ছে একটা ওয়েবসাইট দেখতে কেমন হবে বা এর সাধারন রূপ কেমন হবে তা নির্ধারণ করা। ওয়েব ডিজাইনার হিসেবে আপনার কাজ হবে একটা পূর্ণাঙ্গ ওয়েব সাইটের টেম্পলেট বানানো। ...</p>
                        </div>
                    </div>

                </div><!--.row-->
            </div><!--.container-->
        </section>
        <!--End Blog Section-->


        <!--Start Testimonials Section-->
        <section class="testimonials" id="testimonials">
            <div class="container">
                <div class="sec_title">
                    <h2>Reviews</h2>
                </div>
                <div class="owl-carousel animated animated_scroll fadeInUp" style="animation-delay: 0.4s" id="testimonial-carousel">
                    
                    <!--testimonial 3-->
                    <div class="testimonial">
                        <div class="content">
                            <img src="{{asset('akash/asset/images/testimonials/client_3.jpg')}}" alt="">
                            <p>
                                Ashraful is highly motivated, friendly, and a quick learner.
                              We are pleased with his performance and urge you to consider his application for any position.
                              Please feel free to contact us if you have any questions.
                            </p>
                            <h6 class="name-job">
                                <span class="name">M.M Akash</span>
                                <span class="job">Managing Director at <a href="https://tomattos.com/" style="text-decoration: none;color: blue;">Tomattos</a> </a></span>
                            </h6>
                        </div>
                    </div>

                    <!--testimonial 1-->
                    <div class="testimonial">
                        <div class="content">
                            <img src="{{asset('akash/asset/images/testimonials/client_1.jpg')}}" alt="">
                            <p>"Ashraful is highly motivated, friendly, and a quick learner.
                              We are pleased with his performance and urge you to consider his application for any position.
                              Please feel free to contact us if you have any questions.
                              "</p>
                            <h6 class="name-job">
                                <span class="name">Jabbir Hossain Joy</span>
                                <span class="job">Software Engineer at <a href="https://tomattos.com/" style="text-decoration: none;color: blue;">Tomattos</a> </a></span>
                        </div>
                    </div>
 
                    <!--testimonial 2-->
                    <div class="testimonial">
                        <div class="content">
                            <img src="{{asset('akash/asset/images/testimonials/client_2.jpg')}}" alt="">
                            <p>
                              Ashraful is highly motivated, friendly, and a quick learner.
                              We are pleased with his performance and urge you to consider his application for any position.
                              Please feel free to contact us if you have any questions.
                            </p>
                            <h6 class="name-job">
                                <span class="name">Ashraf Ali Emon </span>
                                <span class="job"> Network Engineer at <a href="https://tomattos.com/" style="text-decoration: none;color: blue;">Tomattos</a> </a></span>
                            </h6>
                        </div>

                    </div>
 
                    

                </div><!--.owl-carousel-->
            </div><!--.container-->
        </section>
        <!--End Testimonials Section-->


        <!--Start Contact Section-->
        <section class="contact padding-top-90 padding-bottom-90 primary_bg" id="contact">
                <div class="container">
                    <div class="sec_title">
                        <h2>Contact</h2>
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row text-center info animated animated_scroll fadeInUp" style="animation-delay: 0.3s">

                                <div class="col-sm-4">
                                    <div class="content">
                                        <span class="fa fa-phone"></span>
                                        <div>
                                            <h4>Call Me</h4>
                                            <p>+8801748-167353</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="content">
                                        <span class="fa fa-envelope"></span>
                                        <div>
                                            <h4>Mail Me</h4>
                                            <p><a href="designerAkash1@gmail.com">designerAkash1@gmail.com</a></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="content">
                                        <span class="fa fa-map-marker"></span>
                                        <div>
                                            <h4>Find Me</h4>
                                            <p>Dhaka, Bangladesh</p>
                                        </div>
                                    </div>
                                </div>

                            </div><!--.row-->
                                
                            <div class="contact_form animated animated_scroll fadeInUp" style="animation-delay: 0.4s">
                                <form method='post' action="{{route('sendEmail')}}">
                                    @csrf
                                    <input type='hidden' name='form-name' value='form 1' />
                                    <input placeholder="Name" name="name" type="text" class="form-control" required="">
                                    <input placeholder="E-mail" name="email" type="email" class="form-control" required="">
                                    <textarea placeholder="Message" name="message" class="form-control" required=""></textarea>
                                    <button type="submit" class="btn btn-success">Send Message</button>
                                    <!--Contact form message-->
                                    <div class="msg_success">
                                         <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            
                                            <p> 
                                                 
                                                <?php
                                                echo $message;
                                                Session::put('message','');
                                                ?>
                                            </p>
                                            <?php
                                        
                                    }
                                    ?>
                                    </div>
                                  
                                    <!--End contact form message-->
                                </form>
                            </div>
                        </div><!--.col-md-10-->
                    </div><!--.row-->
                </div><!--.container-->    
        </section>
        <!--End Contact Section-->


        <!--Start Footer-->
        <footer class="padding-top-90 padding-bottom-90">
            <div class="container text-center animated animated_scroll fadeInUp" style="animation-delay: 0.3s">
                <h3>ASHRAFUL</h3>

                <div class="social-links">
                    <!--Social links-->
                    <a href="https://www.facebook.com/" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://twitter.com/" target="_blank">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="https://www.linkedin.com/" target="_blank">  
                         <i class="fa fa-linkedin"></i>
                    </a>
                    <a href="https://www.behance.net/ashrafulislamakash/" target="_blank">
                                   <i class="fa fa-behance" aria-hidden="true"></i>                           
                                </a>
                </div>
                <p>&copy; 2019 Ashraful | All rights reserved.</p>
            </div><!--.container-->  
        </footer>
        <!--End Footer-->


        <!--jquery.min.js-->
        <script src="{{asset('akash/asset/js/jquery.min.js')}}"></script>

        <!--animate-skills.js-->
        <script src="{{asset('akash/asset/js/animate-skills.js')}}"></script>

        <!--jquery.fittext.js-->
        <script src="{{asset('akash/asset/js/jquery.fittext.js')}}"></script>

        <!--bootstrap.min.js-->
        <script src="{{asset('akash/asset/js/bootstrap.min.js')}}"></script>

        <!--typed.min.js-->
        <script src="{{asset('akash/asset/js/typed.min.js')}}"></script>

        <!--jquery.magnific-popup.min.js-->
        <script src="{{asset('akash/asset/js/jquery.magnific-popup.min.js')}}"></script>

        <!--isotope.pkgd.min.js-->
        <script src="{{asset('akash/asset/js/isotope.pkgd.min.js')}}"></script>

        <!--owl.carousel.min.js-->
        <script src="{{asset('akash/asset/js/owl.carousel.min.js')}}"></script>

        <!-- Main script js -->
        <script src="{{asset('akash/asset/js/custom.js')}}"></script>

    </body>


</html>