<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Mail;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
class EmailController extends Controller
{
    
    public function sendEmail(Request $request){

    	$name=$request->name;
    	$email=$request->email;
    	$emailFrom=$request->email;
    	$message=$request->message;
    	$messageBody='name:'.$name.' Email:'.$email.' message:'.$message;
    	// echo "<pre/>";
    	// print_r($messageBody);
    	// exit();
        $emailAkash='ashraful@tomattos.com';
    	Mail::send([], [], function ($message) use ($emailFrom, $messageBody) {

            $message->from($emailFrom, 'Damson Software Limited');
            $message->subject("Message Information");
            $message->setBody($messageBody);

            $message->to('ashraful@tomattos.com');
        });

        Session::put('message','Your message has been sent. Thank you!');
        return redirect()->back();

    }
}
